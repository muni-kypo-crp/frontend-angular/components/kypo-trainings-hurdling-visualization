export enum LevelTypeEnum {
  Access = 'access',
  Training = 'training',
  Assessment = 'assessment',
  Info = 'info',
}
