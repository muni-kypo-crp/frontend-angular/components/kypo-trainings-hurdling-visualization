import { GenericObject } from './generic-object.type';

export class PlanData {
  keys: string[];
  teams: GenericObject[];
}
