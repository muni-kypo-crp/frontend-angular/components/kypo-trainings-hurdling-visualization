import { GenericObject } from './generic-object.type';

export class PreparedData {
  trainingDataSet: GenericObject[];
  planDataSet: GenericObject[];
}
