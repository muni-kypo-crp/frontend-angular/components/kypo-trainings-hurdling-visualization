import { PlanData } from './plan-data';

export class PlanConfig {
  data: PlanData;
  time: number;
  estimatedTime: number;
}
