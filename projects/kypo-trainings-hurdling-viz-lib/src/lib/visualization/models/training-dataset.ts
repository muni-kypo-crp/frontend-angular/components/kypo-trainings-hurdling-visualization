export class TrainingDataset {
  traineeId: number;
  events: Event[];
  eventsGroups;
  totalTime: number;
}
