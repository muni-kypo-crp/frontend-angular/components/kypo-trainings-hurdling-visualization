export class LevelTimelineData {
  timestamp: number;
  value: string;
  icon: string;
  color: string;
}
