export class TrainingTimeOverviewData {
  start: number;
  end: number;
  levelId: number;
}
