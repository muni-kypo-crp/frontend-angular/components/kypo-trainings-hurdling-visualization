import { Level } from './level';

export class Training {
  id: string;
  name: string;
  levels: Level[];
}
