export enum TraineeView {
  Name = 'name',
  Avatar = 'avatar',
  Both = 'both',
}
