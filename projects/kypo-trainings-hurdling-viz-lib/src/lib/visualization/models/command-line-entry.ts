export class CommandLineEntry {
  timestamp: number;
  command: string;
}
