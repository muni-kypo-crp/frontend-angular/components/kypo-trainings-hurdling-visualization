export class HintDTO {
  hint_id: number;
  hint_title: string;
  hint_content: string;
}
