### 18.0.0 Update to Angular 18.
* 8449c4a -- [CI/CD] Update packages.json version based on GitLab tag.
*   f017d62 -- Merge branch 'develop' into 'master'
|\  
| *   9c52871 -- Merge branch '84-update-to-angular-18' into 'develop'
| |\  
| | * 99cafa3 -- Update to Angular 18
| |/  
|/|   
* | ca99074 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 3336ab5 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   4328e0f -- Merge branch 'update-sentinel-versions' into 'master'
|\ \  
| * | b3b5ca6 -- Update sentinel auth version
|/ /  
* | b7385f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 4a8a7c9 -- [CI/CD] Update packages.json version based on GitLab tag.
* |   8abcaa4 -- Merge branch '83-fix-auto-tag' into 'master'
|\ \  
| * | e24ffa5 -- Update version
|/ /  
* | ecab776 -- Merge branch 'develop' into 'master'
|\| 
| * 38a6c6e -- Update version
| * e3d2aa7 -- Merge branch '82-level-title-in-tooltip-of-headlines-in-progress-of-training-runs-linear' into 'develop'
|/| 
| * 30311fa -- Make timeline show level titles
|/  
* 29d5666 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ad1e17c -- [CI/CD] Update packages.json version based on GitLab tag.
*   50bd72b -- Merge branch '81-update-to-angular-16' into 'master'
|\  
| * 02c6563 -- Update env to run with keycloak
| * 6a7f3db -- Update to Angular 16
|/  
* cd39421 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9de795b -- [CI/CD] Update packages.json version based on GitLab tag.
*   4d5353a -- Merge branch '80-add-parameter-settings-to-visualization-display' into 'master'
|\  
| *   0cc659b -- Finalize changes, restore example dataset
| |\  
| | * ff6fcc5 -- Divide settings into a new component
| * | 719b452 -- Divide settings into a new component
| |/  
| * e150dd7 -- Fix timeline slider values
| * 5cfd750 -- Update dependencies and timeline settings
| * f3651e4 -- Update angular-material slider
| * a1073d9 -- Update dependencies and package.json
| * 211df18 -- Update package-lock
| * 6d75f58 -- Add initial slider to adjust timeline view
| * 2dc8c18 -- Adjust the display of relevent data
| * 3d55c0c -- Enhance legend description
| * 1a84139 -- Add initial inputs
| * 6625918 -- Add upper settings panel
| * 0ab8aa1 -- Add restriction capabilities to progress
|/  
* 4682c3f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 16.0.2 Update sentinel auth version.
* 3336ab5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4328e0f -- Merge branch 'update-sentinel-versions' into 'master'
|\  
| * b3b5ca6 -- Update sentinel auth version
|/  
* b7385f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4a8a7c9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8abcaa4 -- Merge branch '83-fix-auto-tag' into 'master'
|\  
| * e24ffa5 -- Update version
|/  
*   ecab776 -- Merge branch 'develop' into 'master'
|\  
| * 38a6c6e -- Update version
| * e3d2aa7 -- Merge branch '82-level-title-in-tooltip-of-headlines-in-progress-of-training-runs-linear' into 'develop'
|/| 
| * 30311fa -- Make timeline show level titles
|/  
* 29d5666 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ad1e17c -- [CI/CD] Update packages.json version based on GitLab tag.
*   50bd72b -- Merge branch '81-update-to-angular-16' into 'master'
|\  
| * 02c6563 -- Update env to run with keycloak
| * 6a7f3db -- Update to Angular 16
|/  
* cd39421 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9de795b -- [CI/CD] Update packages.json version based on GitLab tag.
*   4d5353a -- Merge branch '80-add-parameter-settings-to-visualization-display' into 'master'
|\  
| *   0cc659b -- Finalize changes, restore example dataset
| |\  
| | * ff6fcc5 -- Divide settings into a new component
| * | 719b452 -- Divide settings into a new component
| |/  
| * e150dd7 -- Fix timeline slider values
| * 5cfd750 -- Update dependencies and timeline settings
| * f3651e4 -- Update angular-material slider
| * a1073d9 -- Update dependencies and package.json
| * 211df18 -- Update package-lock
| * 6d75f58 -- Add initial slider to adjust timeline view
| * 2dc8c18 -- Adjust the display of relevent data
| * 3d55c0c -- Enhance legend description
| * 1a84139 -- Add initial inputs
| * 6625918 -- Add upper settings panel
| * 0ab8aa1 -- Add restriction capabilities to progress
|/  
* 4682c3f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 16.0.1 Change timeline to show level titles.
* 4a8a7c9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8abcaa4 -- Merge branch '83-fix-auto-tag' into 'master'
|\  
| * e24ffa5 -- Update version
|/  
*   ecab776 -- Merge branch 'develop' into 'master'
|\  
| * 38a6c6e -- Update version
| * e3d2aa7 -- Merge branch '82-level-title-in-tooltip-of-headlines-in-progress-of-training-runs-linear' into 'develop'
|/| 
| * 30311fa -- Make timeline show level titles
|/  
* 29d5666 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ad1e17c -- [CI/CD] Update packages.json version based on GitLab tag.
*   50bd72b -- Merge branch '81-update-to-angular-16' into 'master'
|\  
| * 02c6563 -- Update env to run with keycloak
| * 6a7f3db -- Update to Angular 16
|/  
* cd39421 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9de795b -- [CI/CD] Update packages.json version based on GitLab tag.
*   4d5353a -- Merge branch '80-add-parameter-settings-to-visualization-display' into 'master'
|\  
| *   0cc659b -- Finalize changes, restore example dataset
| |\  
| | * ff6fcc5 -- Divide settings into a new component
| * | 719b452 -- Divide settings into a new component
| |/  
| * e150dd7 -- Fix timeline slider values
| * 5cfd750 -- Update dependencies and timeline settings
| * f3651e4 -- Update angular-material slider
| * a1073d9 -- Update dependencies and package.json
| * 211df18 -- Update package-lock
| * 6d75f58 -- Add initial slider to adjust timeline view
| * 2dc8c18 -- Adjust the display of relevent data
| * 3d55c0c -- Enhance legend description
| * 1a84139 -- Add initial inputs
| * 6625918 -- Add upper settings panel
| * 0ab8aa1 -- Add restriction capabilities to progress
|/  
* 4682c3f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* ad1e17c -- [CI/CD] Update packages.json version based on GitLab tag.
*   50bd72b -- Merge branch '81-update-to-angular-16' into 'master'
|\  
| * 02c6563 -- Update env to run with keycloak
| * 6a7f3db -- Update to Angular 16
|/  
* cd39421 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9de795b -- [CI/CD] Update packages.json version based on GitLab tag.
*   4d5353a -- Merge branch '80-add-parameter-settings-to-visualization-display' into 'master'
|\  
| *   0cc659b -- Finalize changes, restore example dataset
| |\  
| | * ff6fcc5 -- Divide settings into a new component
| * | 719b452 -- Divide settings into a new component
| |/  
| * e150dd7 -- Fix timeline slider values
| * 5cfd750 -- Update dependencies and timeline settings
| * f3651e4 -- Update angular-material slider
| * a1073d9 -- Update dependencies and package.json
| * 211df18 -- Update package-lock
| * 6d75f58 -- Add initial slider to adjust timeline view
| * 2dc8c18 -- Adjust the display of relevent data
| * 3d55c0c -- Enhance legend description
| * 1a84139 -- Add initial inputs
| * 6625918 -- Add upper settings panel
| * 0ab8aa1 -- Add restriction capabilities to progress
|/  
* 4682c3f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 15.0.0 Update to Angular 15 and provide settings to adjust visible timeline during training
* 9de795b -- [CI/CD] Update packages.json version based on GitLab tag.
*   4d5353a -- Merge branch '80-add-parameter-settings-to-visualization-display' into 'master'
|\  
| *   0cc659b -- Finalize changes, restore example dataset
| |\  
| | * ff6fcc5 -- Divide settings into a new component
| * | 719b452 -- Divide settings into a new component
| |/  
| * e150dd7 -- Fix timeline slider values
| * 5cfd750 -- Update dependencies and timeline settings
| * f3651e4 -- Update angular-material slider
| * a1073d9 -- Update dependencies and package.json
| * 211df18 -- Update package-lock
| * 6d75f58 -- Add initial slider to adjust timeline view
| * 2dc8c18 -- Adjust the display of relevent data
| * 3d55c0c -- Enhance legend description
| * 1a84139 -- Add initial inputs
| * 6625918 -- Add upper settings panel
| * 0ab8aa1 -- Add restriction capabilities to progress
|/  
* 4682c3f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.2.2 Fix final overview columns responsivity
* a0e26b5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   878415a -- Merge branch '79-fix-hurdling-final-view-responsivity' into 'master'
|\  
| * c8e98ea -- Resolve "Fix hurdling final view responsivity"
|/  
* d088928 -- Update README.md
* a6e708d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.2.1 Fix detailed trainee view display
* fffe964 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7b23cb5 -- Merge branch '78-fix-detailed-trainee-view-display' into 'master'
|\  
| * 8dd07dc -- Resolve "Fix detailed trainee view display"
|/  
* 40a6b2b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.2.0 Enhance the progress visualization logic and appearance.
* 5e6d669 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4408f80 -- Revert version
*   20a663b -- Merge branch 'develop' into 'master'
|\  
| * 5f46082 -- Fix event and level tooltip coordinates for progress visualization. Revert...
|/  
* af7f461 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.1.0 Remove elastic search service url from configs.
* b2ad52f -- [CI/CD] Update packages.json version based on GitLab tag.
*   0e42529 -- Merge branch '76-remove-elastic-search-service-from-configs' into 'master'
|\  
| * 6b83fe0 -- Resolve "Remove elastic search service from configs"
|/  
* e3eecf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.0.1 Rename from kypo2 to kypo.
* e870ae6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8c8e763 -- Merge branch '75-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 9e609ab -- Resolve "Rename from kypo2 to kypo"
|/  
* 937d76c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo2-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 14.0.0 Update to Angular 14
* 8864ba8 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b596ef5 -- Merge branch '74-update-to-angular-14' into 'master'
|\  
| * 03e3270 -- Resolve "Update to Angular 14"
|/  
* 426f8f8 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 13.1.0 Change polling strategy to use max retry attempts and increase timetout on error.
* b958ee1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6a1f787 -- Merge branch '73-change-polling-strategy' into 'master'
|\  
| * 00cd16b -- Resolve "Change polling strategy"
|/  
* c031501 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 13.0.1 Fix event tooltip
* a2c552a -- [CI/CD] Update packages.json version based on GitLab tag.
*   c4d59d7 -- Merge branch '72-fix-event-tooltip' into 'master'
|\  
| * da2f61c -- Resolve "Fix event tooltip"
|/  
* de7ae72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 13.0.0 Update to Angular 13, CI/CD optimization, access level type added
* be35b85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   27c0086 -- Merge branch '71-update-to-angular-13' into 'master'
|\  
| * 1b884a0 -- Resolve "Update to Angular 13"
|/  
*   58dea5b -- Merge branch '70-add-new-level-type-access-level' into 'master'
|\  
| * 8750bbf -- Added a new level type - access level.
|/  
* 4399869 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.8 Fix default player select view
* 9ec7181 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3cbaec2 -- Merge branch '69-fix-default-player-select-view' into 'master'
|\  
| * c56018e -- Resolve "Fix default player select view"
|/  
* 3ebd59d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.7 Fix player selection component trainee name
* 4997bad -- [CI/CD] Update packages.json version based on GitLab tag.
* 907b767 -- Fix player selection
* a320889 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.6 Fix style encapsulation for problematic classes
* bdfcb53 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3d765dd -- Merge branch '67-encapsulate-styles-causing-problems-in-trainings-app' into 'master'
|\  
| * 5027e5e -- Fix styles
|/  
* 77bbcf5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.5 Fix problems with d3v7
* 8daaa90 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69a590d -- Merge branch '66-fix-environment-file' into 'master'
|\  
| * ee68899 -- Resolve "Fix environment file"
|/  
* 50b49f7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.4 Update to d3v7
* 90b734f -- [CI/CD] Update packages.json version based on GitLab tag.
*   616edf9 -- Merge branch '65-bump-d3-version' into 'master'
|\  
| * c750bf7 -- Resolve "Bump d3 version"
|/  
*   0ad1f4b -- Merge branch '64-prepare-visualization-for-dashboard-usage' into 'master'
|\  
| * 7ae2544 -- Resolve "Prepare visualization for dashboard usage"
|/  
*   482cdde -- Merge branch '63-add-license-file' into 'master'
|\  
| * 6570cf1 -- Add license file
|/  
* 71fd645 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.3 Rename terms game to training and flag to answer, new feature - display full estimate, fix local config paths, add build of example in CI 
* 8b80695 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9bf54d2 -- Update VERSION.txt
*   21a4e59 -- Merge branch '62-rename-the-terms-that-have-been-changed-on-backend' into 'master'
|\  
| * 47cb184 -- Resolve "Rename the terms that have been changed on backend"
|/  
*   868f8c6 -- Merge branch '22-hurdling-line-estimates-display' into 'master'
|\  
| * cc4578c -- Resolve "Hurdling line estimates display"
|/  
*   369edc6 -- Merge branch '60-add-build-of-example-app-to-ci' into 'master'
|\  
| * ef0fa0c -- Add build example app
|/  
*   8a61d9f -- Merge branch '59-fix-local-config-paths' into 'master'
|\  
| * 8e24917 -- Fix paths
|/  
* 5f64fb6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
### 12.0.2 Update gitlab CI
* 5e23d64 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2ce99cf -- Merge branch '58-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * c80917a -- Update gitlab CI
|/  
* 92dbdda -- Update project package.json version based on GitLab tag. Done by CI
*   315a877 -- Merge branch '56-add-commands-timeline-to-player-detail-component' into 'master'
|\  
| * 115c6d1 -- Resolve "Add commands timeline to player detail component"
|/  
* a04a961 -- Update project package.json version based on GitLab tag. Done by CI
*   66fd61a -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 71c8aec -- Resolve "Update to Angular 12"
|/  
*   f536586 -- Merge branch '55-fix-avatar-size-in-player-detail' into 'master'
|\  
| * 8e44d84 -- Fix avatar size
|/  
*   a3499ea -- Merge branch '54-integrate-evaluation-notes' into 'master'
|\  
| * f993250 -- Resolve "Integrate evaluation notes"
|/  
*   e68edd7 -- Merge branch '53-add-detailed-view-on-a-selected-player' into 'master'
|\  
| * 10b3676 -- Add player detail component
|/  
* 92c1b3f -- Update project package.json version based on GitLab tag. Done by CI
*   2f924e1 -- Merge branch '52-fix-player-initialization-in-progress-view' into 'master'
|\  
| * cfec3e0 -- Fix player initialization
|/  
* 2a7e3d9 -- Update project package.json version based on GitLab tag. Done by CI
*   74b1224 -- Merge branch 'refactor' into 'master'
|\  
| * a4f8162 -- Integrate new progress visualizations
|/  
*   3df7e67 -- Merge branch '51-update-oidc-configuration' into 'master'
|\  
| * d5e2737 -- Update oidc configuration
|/  
* d88699a -- Update project package.json version based on GitLab tag. Done by CI
*   1e1745c -- Merge branch '50-update-to-angular-11' into 'master'
|\  
| * 9cb704f -- Resolve "Update to Angular 11"
|/  
*   b9e69f3 -- Merge branch '49-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5aa4183 -- recreate package lock
|/  
*   41924b1 -- Merge branch '47-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 98e05a8 -- Resolve "Migrate from tslint to eslint"
|/  
* b324e41 -- Update project package.json version based on GitLab tag. Done by CI
*   ed6b64d -- Merge branch '48-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * c36f09c -- Rename package scope and update dependencies
|/  
* 7ab8986 -- Update project package.json version based on GitLab tag. Done by CI
*   ab04bb5 -- Merge branch '46-update-dependencies-to-new-format' into 'master'
|\  
| * 3b8ea28 -- Rename dependencies
|/  
* a5d17bb -- Update project package.json version based on GitLab tag. Done by CI
*   a8099b9 -- Merge branch '45-rename-package-to-kypo-hurdling-visualization' into 'master'
|\  
| * 3f8610b -- Rename the package
|/  
*   5504b04 -- Merge branch '43-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 997b9cc -- Remove personal data
| * 49ac3c6 -- Remove personal info
| * a322f75 -- Clear personal data from readme
| * 6ff5133 -- Clear personal data from mocks
|/  
* 536cc0d -- Update project package.json version based on GitLab tag. Done by CI
*   7e71b87 -- Merge branch '40-replace-dependency-on-kypo-auth-with-sentinel-auth' into 'master'
|\  
| * f96b45b -- Resolve "Replace dependency on kypo-auth with sentinel-auth"
|/  
* d111d53 -- Update project package.json version based on GitLab tag. Done by CI
*   dd7a8da -- Merge branch '38-update-endpoint-for-training-events' into 'master'
|\  
| * 7e1b268 -- Updated training-events to training-platform-events to match new backend endpoint
|/  
*   5213833 -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 12374a5 -- Resolve "Use Cypress image in CI"
|/  
* 82af49a -- Update project package.json version based on GitLab tag. Done by CI
*   23b0ab3 -- Merge branch '36-update-config-to-support-new-elasticsearch-backend-service' into 'master'
|\  
| * fe6f259 -- Resolve "Update config to support new elasticsearch backend service"
|/  
* e925995 -- Update project package.json version based on GitLab tag. Done by CI
*   ac8cb2d -- Merge branch '35-update-to-angular-10' into 'master'
|\  
| * 9115758 -- Resolve "Update to Angular 10"
|/  
* d87c60c -- Merge branch '34-make-the-ci-build-stage-build-with-prod-param' into 'master'
* 4a40e9f -- Resolve "Make the CI build stage build with prod param"
