18.0.0 Update to Angular 18.
16.0.2 Update sentinel auth version.
16.0.1 Change timeline to show level titles.
16.0.0 Update to Angular 16 and update local issuer to keycloak.
15.0.0 Update to Angular 15 and provide settings to adjust visible timeline during training
14.2.2 Fix final overview columns responsivity
14.2.1 Fix detailed trainee view display
14.2.0 Enhance the progress visualization logic and appearance.
14.1.0 Remove elastic search service url from configs.
14.0.1 Rename from kypo2 to kypo.
14.0.0 Update to Angular 14
13.1.0 Change polling strategy to use max retry attempts and increase timetout on error.
13.0.1 Fix event tooltip
13.0.0 Update to Angular 13, CI/CD optimization, access level type added
12.0.8 Fix default player select view
12.0.7 Fix player selection component trainee name
12.0.6 Fix style encapsulation for problematic classes
12.0.5 Fix problems with d3v7
12.0.4 Update to d3v7
12.0.3 Rename terms game to training and flag to answer, new feature - display full estimate, fix local config paths, add build of example in CI
12.0.2 Update gitlab CI
